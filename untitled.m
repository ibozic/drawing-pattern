function varargout = untitled(varargin)
% UNTITLED M-file for untitled.fig
%      UNTITLED, by itself, creates a new UNTITLED or raises the existing
%      singleton*.
%
%      H = UNTITLED returns the handle to a new UNTITLED or the handle to
%      the existing singleton*.
%
%      UNTITLED('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UNTITLED.M with the given input arguments.
%
%      UNTITLED('Property','Value',...) creates a new UNTITLED or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before untitled_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to untitled_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help untitled

% Last Modified by GUIDE v2.5 07-Oct-2008 11:04:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @untitled_OpeningFcn, ...
                   'gui_OutputFcn',  @untitled_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before untitled is made visible.
function untitled_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to untitled (see VARARGIN)

% Choose default command line output for untitled
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes untitled wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = untitled_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%saveas(handles.axes1,'studija.jpg');
%hgexport(handles.axes1,'studija.jpg');
set(handles.uipanel4,'Visible','off');
set(handles.uipanel9,'Visible','off');
set(handles.uipanel13,'Visible','on');
set(handles.pushbutton1,'Enable','off');
guidata(hObject, handles);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.A=uigetfile('*.txt');
if handles.A==0
   set(handles.pushbutton2,'Value',[0,0]); 
else
BB=importdata(handles.A);
handles.matrica=BB.data;
handles.x1=handles.matrica(:,1);
handles.y1=handles.matrica(:,2);
handles.x=handles.x1-handles.x1(1,1);
handles.y=handles.y1-handles.y1(1,1);
handles.n=length(handles.x');
plot([0,0],[0,216]);hold on;plot(handles.x,handles.y,'r');axis([-75 75 -10 250]);hold off
set(handles.pushbutton2,'Visible','off');
set(handles.pushbutton1,'Visible','on');
set(handles.pushbutton3,'Visible','on');
set(handles.uipanel4,'Visible','on');
end
guidata(hObject, handles);

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%handles.B=[handles.A, 'rezultati'];
set(handles.uipanel4,'Visible','off');
set(handles.uipanel9,'Visible','off');
set(handles.uipanel12,'Visible','on');
guidata(hObject, handles);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
set(handles.uipanel5,'Visible','on');
handles.izbor='Po liniji';
else
set(handles.uipanel5,'Visible','off');
end
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of radiobutton1


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
set(handles.uipanel5,'Visible','on');
handles.izbor='Izmedju dve tacke';
else
set(handles.uipanel5,'Visible','off');
end
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
handles.dominantna='Dominantna';
set(handles.radiobutton5,'Enable','on');
set(handles.radiobutton6,'Enable','on');
else
set(handles.radiobutton5,'Enable','off');
set(handles.radiobutton6,'Enable','off');
end
handles.merenje=0;
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of radiobutton3


% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
handles.dominantna='Nedominantna';
set(handles.radiobutton5,'Enable','on');
set(handles.radiobutton6,'Enable','on');
else
set(handles.radiobutton5,'Enable','off');
set(handles.radiobutton6,'Enable','off');
end
handles.merenje=1;
guidata(hObject, handles)
% Hint: get(hObject,'Value') returns toggle state of radiobutton4


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
set(handles.pushbutton7,'Enable','on');
handles.tacke='Leva ruka';
else
set(handles.pushbutton7,'Enable','off');
end
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of radiobutton5


% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
set(handles.pushbutton7,'Enable','on');
handles.tacke='Desna ruka';
else
set(handles.pushbutton7,'Enable','off');
end
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of radiobutton6


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton6,'Visible','off');
set(handles.pushbutton7,'Visible','off');
set(handles.uipanel8,'Visible','on');

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ((get(hObject,'Value') == get(hObject,'Max'))&&(get(handles.checkbox2,'Value')==get(handles.checkbox2,'Min'))&&(get(handles.checkbox3,'Value')==get(handles.checkbox3,'Min')))

    set(handles.pushbutton4,'Enable','on');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    handles.iicheckbox1=1;
    handles.iicheckbox2=0;
    handles.iicheckbox3=0;
    handles.iicheckbox4=0;
    handles.setup1=1;
    handles.setup2=0;
    handles.setup3=0;
    handles.setup4=0;
    
elseif ((get(hObject,'Value') == get(hObject,'Max'))&&(get(handles.checkbox2,'Value')==get(handles.checkbox2,'Max')))
        
        set(handles.pushbutton4,'Enable','on');
        set(handles.checkbox3,'Enable','off');
        set(handles.checkbox4,'Enable','off');
        handles.setup1=1;
        handles.setup2=1;
        handles.setup3=0;
        handles.setup4=0;
        handles.iicheckbox1=1;
        handles.iicheckbox2=1;
        handles.iicheckbox3=0;
        handles.iicheckbox4=0;
        
elseif  ((get(hObject,'Value') == get(hObject,'Max'))&&(get(handles.checkbox3,'Value')==get(handles.checkbox3,'Max')))
    
    
        set(handles.pushbutton4,'Enable','on');
        set(handles.checkbox2,'Enable','off');
        set(handles.checkbox4,'Enable','off');
        handles.setup1=1;
        handles.setup2=0;
        handles.setup3=1;
        handles.setup4=0;
        handles.iicheckbox1=1';
        handles.iicheckbox3=1;
        handles.iicheckbox2=0;
        handles.iicheckbox4=0;   
        
elseif  ((get(hObject,'Value') == get(hObject,'Min'))&&(get(handles.checkbox2,'Value')==get(handles.checkbox2,'Max')))  
    
    
        set(handles.pushbutton4,'Enable','on');
        set(handles.checkbox3,'Enable','off');
        set(handles.checkbox4,'Enable','off');
        handles.setup1=0;
        handles.setup2=1;
        handles.setup3=0;
        handles.setup4=0;
        handles.iicheckbox1=0;
        handles.iicheckbox2=1;
        handles.iicheckbox3=0;
        handles.iicheckbox4=0;
    
elseif  ((get(hObject,'Value') == get(hObject,'Min'))&&(get(handles.checkbox3,'Value')==get(handles.checkbox3,'Max')))    
    
    
        set(handles.pushbutton4,'Enable','on');
        set(handles.checkbox2,'Enable','off');
        set(handles.checkbox4,'Enable','off');
        handles.setup1=0;
        handles.setup2=0;
        handles.setup3=1;
        handles.setup4=0;
        handles.iicheckbox1=0;
        handles.iicheckbox3=1;
        handles.iicheckbox2=0;
        handles.iicheckbox4=0; 
        
elseif ((get(hObject,'Value') == get(hObject,'Min'))&&(get(handles.checkbox2,'Value')==get(handles.checkbox2,'Min'))&&(get(handles.checkbox2,'Value')==get(handles.checkbox3,'Min')))
    
        set(handles.pushbutton4,'Enable','off');
        set(handles.checkbox1,'Enable','on');
        set(handles.checkbox2,'Enable','on');
        set(handles.checkbox3,'Enable','on');
        set(handles.checkbox4,'Enable','on');
        handles.setup1=0;
        handles.setup2=0;
        handles.setup3=0;
        handles.setup4=0;
        handles.iicheckbox1=0;
        handles.iicheckbox2=0;
        handles.iicheckbox3=0;
        handles.iicheckbox4=0;
        
end

guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ((get(hObject,'Value') == get(hObject,'Max'))&&(get(handles.checkbox1,'Value')==get(handles.checkbox1,'Min')))

    set(handles.pushbutton4,'Enable','on');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    handles.iicheckbox1=0;    
    handles.iicheckbox2=1;
    handles.iicheckbox3=0;
    handles.iicheckbox4=0; 
    handles.setup1=0;
    handles.setup2=1;
    handles.setup4=0;
    handles.setup3=0;

elseif ((get(hObject,'Value') == get(hObject,'Min'))&&(get(handles.checkbox1,'Value')==get(handles.checkbox1,'Min')))

        set(handles.pushbutton4,'Enable','off');
        set(handles.checkbox1,'Enable','on');
        set(handles.checkbox3,'Enable','on');
        set(handles.checkbox4,'Enable','on');
        handles.iicheckbox1=0;    
        handles.iicheckbox2=0;
        handles.iicheckbox3=0;
        handles.iicheckbox4=0;    
        handles.setup1=0;
        handles.setup2=0;
        handles.setup4=0;
        handles.setup3=0;
        
elseif ((get(hObject,'Value') == get(hObject,'Max'))&&(get(handles.checkbox1,'Value')==get(handles.checkbox1,'Max')))

        set(handles.pushbutton4,'Enable','on');
        set(handles.checkbox3,'Enable','off');
        set(handles.checkbox4,'Enable','off');
        handles.iicheckbox1=1;    
        handles.iicheckbox2=1;
        handles.iicheckbox3=0;
        handles.iicheckbox4=0;          
        handles.setup1=1;
        handles.setup2=1;
        handles.setup4=0;
        handles.setup3=0;
        
elseif ((get(hObject,'Value') == get(hObject,'Min'))&&(get(handles.checkbox1,'Value')==get(handles.checkbox1,'Max')))

        set(handles.pushbutton4,'Enable','on');
        set(handles.checkbox3,'Enable','on');
        set(handles.checkbox4,'Enable','off');
        handles.iicheckbox1=1;    
        handles.iicheckbox2=0;
        handles.iicheckbox3=0;
        handles.iicheckbox4=0;          
        handles.setup1=1;
        handles.setup2=0;
        handles.setup4=0;
        handles.setup3=0;


end

guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ((get(hObject,'Value') == get(hObject,'Max'))&&(get(handles.checkbox1,'Value')==get(handles.checkbox1,'Min')))

    set(handles.pushbutton4,'Enable','on');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    handles.iicheckbox1=0;    
    handles.iicheckbox3=1;
    handles.iicheckbox2=0;
    handles.iicheckbox4=0; 
    handles.setup1=0;
    handles.setup2=0;
    handles.setup4=0;
    handles.setup3=1;

elseif ((get(hObject,'Value') == get(hObject,'Min'))&&(get(handles.checkbox1,'Value')==get(handles.checkbox1,'Min')))

        set(handles.pushbutton4,'Enable','off');
        set(handles.checkbox1,'Enable','on');
        set(handles.checkbox2,'Enable','on');
        set(handles.checkbox4,'Enable','on');
        handles.iicheckbox1=0;    
        handles.iicheckbox2=0;
        handles.iicheckbox3=0;
        handles.iicheckbox4=0;    
        handles.setup1=0;
        handles.setup2=0;
        handles.setup4=0;
        handles.setup3=0;
        
elseif ((get(hObject,'Value') == get(hObject,'Max'))&&(get(handles.checkbox1,'Value')==get(handles.checkbox1,'Max')))

        set(handles.pushbutton4,'Enable','on');
        set(handles.checkbox2,'Enable','off');
        set(handles.checkbox4,'Enable','off');
        handles.iicheckbox1=1;    
        handles.iicheckbox3=1;
        handles.iicheckbox2=0;
        handles.iicheckbox4=0;          
        handles.setup1=1;
        handles.setup2=0;
        handles.setup4=0;
        handles.setup3=1;
        
elseif ((get(hObject,'Value') == get(hObject,'Min'))&&(get(handles.checkbox1,'Value')==get(handles.checkbox1,'Max')))

        set(handles.pushbutton4,'Enable','on');
        set(handles.checkbox2,'Enable','on');
        set(handles.checkbox4,'Enable','off');
        handles.iicheckbox1=1;    
        handles.iicheckbox2=0;
        handles.iicheckbox3=0;
        handles.iicheckbox4=0;          
        handles.setup1=1;
        handles.setup2=0;
        handles.setup4=0;
        handles.setup3=0;

end

guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
set(handles.pushbutton4,'Enable','on');
set(handles.checkbox1,'Enable','off');
set(handles.checkbox2,'Enable','off');
set(handles.checkbox3,'Enable','off');
handles.iicheckbox4=1;
handles.iicheckbox2=0;
handles.iicheckbox3=0;
handles.iicheckbox1=0;   
handles.setup4=1;
handles.setup1=0;
handles.setup2=0;
handles.setup3=0;
else
set(handles.checkbox3,'Enable','on');
set(handles.checkbox2,'Enable','on');
set(handles.checkbox1,'Enable','on');
set(handles.pushbutton4,'Enable','off');
handles.iicheckbox4=0;
handles.iicheckbox2=0;
handles.iicheckbox3=0;
handles.iicheckbox1=0; 
handles.setup4=0;
handles.setup1=0;
handles.setup2=0;
handles.setup3=0;
end
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of checkbox4

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton1,'Enable','on');
set(handles.pushbutton3,'Enable','on');
if handles.setup1==1
%   if handles.setup2==1  
%         plot([0,handles.x(handles.n,1)],[0,handles.y(handles.n,1)],'g');hold on;plot([0,handles.x(handles.n,1)],[handles.y(handles.n,1),handles.y(handles.n,1)],'k-.');hold on;plot([handles.x(handles.n,1),handles.x(handles.n,1)],[handles.y(handles.n,1),216],'k-.');hold on;plot([0,0],[0,216]);hold on;plot(handles.x,handles.y,'r');axis([-75 75 -10 250]);hold on;plot(handles.x(kordinata(2:t),1),handles.y(kordinata(2:t),1),'b*');hold on;plot(handles.x(handles.n,1),handles.y(handles.n,1),'b*');hold off
%   else
      plot([0,handles.x(handles.n,1)],[handles.y(handles.n,1),handles.y(handles.n,1)],'k-.');hold on;plot([handles.x(handles.n,1),handles.x(handles.n,1)],[handles.y(handles.n,1),216],'k-.');hold on;plot([0,0],[0,216]);hold on;plot(handles.x,handles.y,'r');axis([-75 75 -10 250]);hold off
%   end
      

handles.n=length(handles.x');
handles.xm=(1/handles.n)*sum(sum(handles.x));
if handles.merenje==0
handles.YM=216-handles.y(handles.n,1);
handles.XM=handles.x(handles.n,1);
elseif handles.merenje==1
handles.YM=216-handles.y(handles.n,1);
handles.XM=handles.x(handles.n,1);
end
for i=1:handles.n
    handles.x2=(handles.x(i)-handles.xm)^2;
end
set(handles.text6,'Visible','on');
set(handles.text7,'Visible','on');
handles.x3=sum(sum(handles.x2));
handles.E=(1/(handles.n-1))*handles.x3;
handles.e=sqrt(handles.E);
set(handles.uipanel9,'Visible','on')
set(handles.uipanel5,'Visible','off')
set(handles.pushbutton4,'Visible','off');
set(handles.text1,'Enable','on');
set(handles.edit1,'Visible','on');
set(handles.text5,'Enable','on');
set(handles.edit1,'String',handles.XM);
set(handles.text2,'Enable','on');
set(handles.edit2,'Visible','on');
set(handles.text4,'Enable','on');
set(handles.edit2,'String',handles.YM);
set(handles.text3,'Enable','on');
set(handles.edit3,'Visible','on');
set(handles.edit3,'String',handles.e);
else
end


if handles.setup2==1

    a=handles.x(handles.n,1);
    b=handles.y(handles.n,1);
    handles.tangens=atan(a/b)*180/pi;
    yn=(b/a)*handles.x;
% Dodati ostatak
    razlika=yn-handles.y;
    brojac=0;
    kordinata=0;
    for i=1:(handles.n-1)
        if((razlika(i)<0 && razlika(i+1)>0)||(razlika(i)>0 && razlika(i+1)<0))
            brojac=brojac+1;
            kordinata=[kordinata,i];
        end
    end
t=length(kordinata); 

    if handles.setup1==1
        plot([0,handles.x(handles.n,1)],[0,handles.y(handles.n,1)],'g');hold on;plot([0,handles.x(handles.n,1)],[handles.y(handles.n,1),handles.y(handles.n,1)],'k-.');hold on;plot([handles.x(handles.n,1),handles.x(handles.n,1)],[handles.y(handles.n,1),216],'k-.');hold on;plot([0,0],[0,216]);hold on;plot(handles.x,handles.y,'r');axis([-75 75 -10 250]);hold on;plot(handles.x(kordinata(2:t),1),handles.y(kordinata(2:t),1),'b*');hold on;plot(handles.x(handles.n,1),handles.y(handles.n,1),'b*');hold off
    else
    plot([0,handles.x(handles.n,1)],[0,handles.y(handles.n,1)],'g');hold on;plot([0,0],[0,216]);hold on;plot(handles.x,handles.y,'r');axis([-75 75 -10 250]);hold on;plot(handles.x(kordinata(2:t),1),handles.y(kordinata(2:t),1),'b*');hold on;plot(handles.x(handles.n,1),handles.y(handles.n,1),'b*');hold off
    end
    handles.nule=brojac+1;
%%%%%%%%%%%%%%
%%%%%%%%%%%%%%
    set(handles.uipanel9,'Visible','on')
    set(handles.uipanel5,'Visible','off')
    set(handles.pushbutton4,'Visible','off');
   if (get(handles.checkbox1,'Value')==get(handles.checkbox1,'Max'))
       handles.n=length(handles.x');
       handles.xm=(1/handles.n)*sum(sum(handles.x));
       if handles.merenje==0
       handles.YM=216-handles.y(handles.n,1);
       handles.XM=handles.x(handles.n,1);
       elseif handles.merenje==1
       handles.YM=216-handles.y(handles.n,1);
       handles.XM=handles.x(handles.n,1);
       end
       for i=1:handles.n
       handles.x2=(handles.x(i)-handles.xm)^2;
       end
       handles.x3=sum(sum(handles.x2));
       handles.E=(1/(handles.n-1))*handles.x3;
       handles.e=sqrt(handles.E);
       set(handles.text1,'Enable','on');
       set(handles.edit1,'Visible','on');
       set(handles.text5,'Enable','on');
       set(handles.edit1,'String',handles.XM);
       set(handles.text2,'Enable','on');
       set(handles.edit2,'Visible','on');
       set(handles.text4,'Enable','on');
       set(handles.edit2,'String',handles.YM);
       set(handles.text3,'Enable','on');
       set(handles.edit3,'Visible','on');
       set(handles.edit3,'String',handles.e);
       set(handles.text6,'Visible','on');
       set(handles.text7,'Visible','on');
       set(handles.text6,'Enable','on');
       set(handles.edit4,'Visible','on');
       set(handles.text7,'Enable','on');
       set(handles.edit4,'String',handles.tangens);
       set(handles.text10,'Visible','on');
       set(handles.text10,'Enable','on');
       set(handles.edit5,'Visible','on');
       set(handles.edit5,'String',handles.nule);
       
   else
       set(handles.text6,'Visible','on');
       set(handles.text6,'Enable','on');
       set(handles.edit4,'Visible','on');
       set(handles.text7,'Visible','on');
       set(handles.text7,'Enable','on');
       set(handles.edit4,'String',handles.tangens);
       set(handles.text10,'Visible','on');
       set(handles.text10,'Enable','on');
       set(handles.edit5,'Visible','on');
       set(handles.edit5,'String',handles.nule);
   end
end

if handles.setup3==1
    
    
m=handles.n-1;
m1=(m/2);
if (m1-floor(m1)>=0.5)
    mm=ceil(m1);
else
    mm=floor(m1);
end
mmm=[1,mm,handles.n];
for i=1:length(mmm)
xm(i)=handles.x(mmm(i),1);
ym(i)=handles.y(mmm(i),1);
end
yy = spline(xm,ym,handles.x);

    razlika1=handles.y-yy;
    brojac1=0;
    kordinata1=0;
    for i=1:(handles.n-1)
        if((razlika1(i)<0 && razlika1(i+1)>0)||(razlika1(i)>0 && razlika1(i+1)<0))
            brojac1=brojac1+1;
            kordinata1=[kordinata1,i];
        end
    end
t1=length(kordinata1); 

    if handles.setup1==1
        plot(handles.x,yy,'g');hold on;plot([0,handles.x(handles.n,1)],[handles.y(handles.n,1),handles.y(handles.n,1)],'k-.');hold on;plot([handles.x(handles.n,1),handles.x(handles.n,1)],[handles.y(handles.n,1),216],'k-.');hold on;plot([0,0],[0,216]);hold on;plot(handles.x,handles.y,'r');axis([-75 75 -10 250]);hold on;plot(handles.x(kordinata1(2:t1),1),handles.y(kordinata1(2:t1),1),'b*');hold on;plot(handles.x(handles.n,1),handles.y(handles.n,1),'b*');hold off
    else
    plot(handles.x,yy,'g');hold on;plot([0,0],[0,216]);hold on;plot(handles.x,handles.y,'r');axis([-75 75 -10 250]);hold on;plot(handles.x(kordinata1(2:t1),1),handles.y(kordinata1(2:t1),1),'b*');hold on;plot(handles.x(handles.n,1),handles.y(handles.n,1),'b*');hold off
    end
    handles.nule3=brojac1;
y11=handles.y';
 yk1=[y11(1),y11];
 yk=[yk1,y11(handles.n)];
 h=0.001;
 nk=length(yk);
 for sss=2:nk-1
     yizvod=(yk(sss-1)-2*yk(sss)+yk(sss+1)/h^2);
 end
 kr=1./yizvod;
 
    handles.zakrivljenost=max(max(kr));
        set(handles.uipanel9,'Visible','on')
        set(handles.uipanel5,'Visible','off')
        set(handles.pushbutton4,'Visible','off');
   if (get(handles.checkbox1,'Value')==get(handles.checkbox1,'Max'))
       handles.n=length(handles.x');
       handles.xm=(1/handles.n)*sum(sum(handles.x));
       if handles.merenje==0
       handles.YM=216-handles.y(handles.n,1);
       handles.XM=handles.x(handles.n,1);
       elseif handles.merenje==1
       handles.YM=216-handles.y(handles.n,1);
       handles.XM=handles.x(handles.n,1);
       end
       for i=1:handles.n
       handles.x2=(handles.x(i)-handles.xm)^2;
       end
       handles.x3=sum(sum(handles.x2));
       handles.E=(1/(handles.n-1))*handles.x3;
       handles.e=sqrt(handles.E);
       set(handles.text1,'Enable','on');
       set(handles.edit1,'Visible','on');
       set(handles.text5,'Enable','on');
       set(handles.edit1,'String',handles.XM);
       set(handles.text2,'Enable','on');
       set(handles.edit2,'Visible','on');
       set(handles.text4,'Enable','on');
       set(handles.edit2,'String',handles.YM);
       set(handles.text3,'Enable','on');
       set(handles.edit3,'Visible','on');
       set(handles.edit3,'String',handles.e);
       set(handles.text8,'Visible','on');
       set(handles.text9,'Visible','on');
       set(handles.text8,'Enable','on');
       set(handles.edit4,'Visible','on');
       set(handles.text9,'Enable','on');
       set(handles.edit4,'String',handles.zakrivljenost);
       set(handles.text10,'Visible','on');
       set(handles.text10,'Enable','on');
       set(handles.edit5,'Visible','on');
       set(handles.edit5,'String',handles.nule3);
   else
       set(handles.text8,'Visible','on');
       set(handles.text9,'Visible','on');
       set(handles.text8,'Enable','on');
       set(handles.edit4,'Visible','on');
       set(handles.text9,'Enable','on');
       set(handles.edit4,'String',handles.zakrivljenost);
       set(handles.text10,'Visible','on');
       set(handles.text10,'Enable','on');
       set(handles.edit5,'Visible','on');
       set(handles.edit5,'String',handles.nule3);
   end
end
guidata(hObject, handles);



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.uipanel5,'Visible','off')
set(handles.uipanel10,'Visible','on');
guidata(hObject, handles);



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double
handles.ime=(get(hObject,'String'));
set(handles.edit8,'Enable','on');
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
handles.prezime=(get(hObject,'String'));
set(handles.edit9,'Enable','on');
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double
handles.ddr=(get(hObject,'String'));
handles.ddr=str2double(handles.ddr);

if ((isnan(handles.ddr) && ~(isinteger(handles.ddr))) || handles.ddr<1 || handles.ddr>31)
    
    errordlg('Morate uneti celobrojnu vrednost izmedju 1 i 31','Pogresan unos','Modal');
    set(handles.edit9,'String','');
    set(handles.ddr,'Value',0);
    return

else
end
set(handles.edit10,'Enable','on');
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double
handles.mdr=(get(hObject,'String'));
handles.mdr=str2double(handles.mdr);

if ((isnan(handles.mdr) && ~(isinteger(handles.mdr))) || handles.mdr<1 || handles.mdr>12)
    
    errordlg('Morate uneti celobrojnu vrednost izmedju 1 i 12','Pogresan unos','Modal');
    set(handles.edit10,'String','');
%     set(handles.mdr,'Value',0);
    return  

else
end
set(handles.edit11,'Enable','on');
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double
handles.gdr=(get(hObject,'String'));
handles.gdr=str2double(handles.gdr);

if ((isnan(handles.gdr) && ~(isinteger(handles.gdr))) || handles.gdr<1900 || handles.gdr>2008)
    
    errordlg('Morate uneti celobrojnu vrednost izmedju 1900 i 2008','Pogresan unos','Modal');
    set(handles.edit11,'String','');
    set(handles.gdr,'Value',0);
    return

else
end
set(handles.edit12,'Enable','on');
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double
handles.dds=(get(hObject,'String'));
handles.dds=str2double(handles.dds);

if ((isnan(handles.dds) && ~(isinteger(handles.dds))) || handles.dds<1 || handles.dds>31)
    
    errordlg('Morate uneti celobrojnu vrednost izmedju 1 i 31','Pogresan unos','Modal');
    set(handles.edit12,'String','');
    set(handles.dds,'Value',0);
    return

else
end
set(handles.edit13,'Enable','on');
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double
handles.mds=(get(hObject,'String'));
handles.mds=str2double(handles.mds);

if ((isnan(handles.mds) && ~(isinteger(handles.mds))) || handles.mds<1 || handles.mds>12)
    
    errordlg('Morate uneti celobrojnu vrednost izmedju 1 i 12','Pogresan unos','Modal');
    set(handles.edit13,'String','');
    set(handles.mds,'Value',0);
    return

else
end
set(handles.edit14,'Enable','on');
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double
handles.gds=(get(hObject,'String'));
handles.gds=str2double(handles.gds);

if ((isnan(handles.gds) && ~(isinteger(handles.gds))) || handles.gds<1900)
    
    errordlg('Morate uneti celobrojnu vrednost izmedju 1900 i 2008','Pogresan unos','Modal');
    set(handles.edit14,'String','');
    set(handles.gds,'Value',0);
    return
    
elseif handles.gds-handles.gdr<0
    
    errordlg('Pogresno uneta godina snimanja','Pogresan unos','Modal');
    set(handles.edit14,'String','');
    set(handles.gds,'Value',0);
    return
    
elseif ((handles.gds-handles.gdr==0)&& (handles.mds-handles.mdr<0))
    errordlg('Pogresno unet mesec snimanja','Pogresan unos','Modal');
    set(handles.edit13,'String','');
    set(handles.mds,'Value',0);
    return
    
elseif ((handles.gds-handles.gdr==0) && (handles.mds-handles.mdr==0) && (handles.dds-handles.ddr<0))
    errordlg('Pogresno unet dan snimanja','Pogresan unos','Modal');
    set(handles.edit12,'String','');
    set(handles.dds,'Value',0);
    return    

elseif ((handles.gds-handles.gdr<0) && (handles.mds-handles.mdr<0) && (handles.dds-handles.ddr<0))
    errordlg('Pogresno unet datum snimanja','Pogresan unos','Modal');
    set(handles.edit12,'String','');
    set(handles.edit13,'String','');
    set(handles.edit14,'String','');
    set(handles.dds,'Value',0);
    set(handles.mds,'Value',0);
    set(handles.gds,'Value',0);
    return    

end
set(handles.pushbutton9,'Enable','on');
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.uipanel10,'Visible','off');
set(handles.uipanel5,'Visible','on');
set(handles.pushbutton7,'Enable','off');
set(handles.pushbutton6,'Enable','on');
set(handles.edit7,'String','');
set(handles.edit8,'String','');
set(handles.edit9,'String','');
set(handles.edit10,'String','');
set(handles.edit11,'String','');
set(handles.edit12,'String','');
set(handles.edit13,'String','');
set(handles.edit14,'String','');
guidata(hObject,handles);



function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit23 as text
%        str2double(get(hObject,'String')) returns contents of edit23 as a double
handles.nazivfajla=(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.uipanel12,'Visible','off');
tekst0='Rezultati studije';
tekst='Odstupanje po x koordinati:';
tekst1='Odstupanje po y koordinati:';
tekst2='Standardna devijacija:';
tekst3='Standardni setup:';
tekst4='Setup 1 (ugao, broj preseka):';
tekst5='Odstupanje od normalnog pravca:';
tekst6='Broj preseka:';
tekst7='Setup 2 (zakrivljenost, broj preseka):';
tekst8='Zakrivljenost krive:';
tekst9='Setup 3 (povrsina figure):';
tekst10='Povrsina objekta:';
tekst11='Ime:';
tekst12='Prezime:';
tekst13='Datum rodjenja:';
tekst14='Datum snimanja:';
tekst15='Tip studije:';
tekst16='Ruka:';
tekst17='Osobina ruke:';

nazivfajla=[handles.nazivfajla,'.txt'];

dominantna=handles.dominantna;
tacke=handles.tacke;
izbor=handles.izbor;

ime=handles.ime;
prezime=handles.prezime;

ddr=handles.ddr;
mdr=handles.mdr;
gdr=handles.gdr;

dds=handles.dds;
mds=handles.mds;
gds=handles.gds;

setup1=handles.iicheckbox1;
if setup1==1
    trazeno1='Trazena';
x=handles.XM;
y=handles.YM;
e=handles.e;
else
    trazeno1='Nije trazen';
x=0.;
y=0.;
e=0.;
end

setup2=handles.iicheckbox2;
if setup2==1
    trazeno2='Trazena';
tangens=handles.tangens;
nule=handles.nule;
else
    trazeno2='Nije trazena';
tangens=0.;
nule=0;
end

setup3=handles.iicheckbox3;
if setup3==1
    trazeno3='Trazena';
zakrivljenost=handles.zakrivljenost;
nule3=0;
else
    trazeno3='Nije trazena';
zakrivljenost=0.;
nule3=0;
end

setup4=handles.iicheckbox4;
if setup4==1
    trazeno4='Trazena';
povrsina=handles.povrsina;
else
    trazeno4='Nije trazena';
povrsina=0.;
end

fid=fopen(nazivfajla,'w');
fprintf(fid,'\t\t\t\t%s\n\n\n\n %s\t\t %s\n %s    %s\n\n\n %s\t %i.  %i.  %i.\n %s\t %i.  %i.  %i.\n\n\n %s\t  %s\n %s\t\t  %s\n %s\t  %s\n\n\n %s\t\t\t\t       %s\n\n \t%s        %8.6f\n \t%s        %8.6f\n \t%s\t\t     %8.6f\n\n\n %s\t\t       %s\n\n \t%s  %8.6f\n \t%s\t\t\t     %i\n\n\n %s\t       %s\n\n \t%s\t\t %8.2f\n \t%s\t\t\t     %i\n\n\n %s\t\t\t       %s\n\n \t%s\t\t\t    %8.5f mm2',tekst0,tekst11,ime,tekst12,prezime,tekst13,ddr,mdr,gdr,tekst14,dds,mds,gds,tekst15,izbor,tekst16,tacke,tekst17,dominantna,tekst3,trazeno1,tekst,x,tekst1,y,tekst2,e,tekst4,trazeno2,tekst5,tangens,tekst6,nule,tekst7,trazeno3,tekst8,zakrivljenost,tekst6,nule3,tekst9,trazeno4,tekst10,povrsina);
fclose(fid);
set(handles.pushbutton3,'Enable','off');
set(handles.edit23,'String','');
guidata(hObject,handles);



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit24 as text
%        str2double(get(hObject,'String')) returns contents of edit24 as a double
handles.nazivfajlaslike=(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.uipanel13,'Visible','off');
naziv=[handles.nazivfajlaslike,'.bmp'];
print(naziv,'-dbmp16m');
set(handles.edit24,'String','');
set(handles.pushbutton1,'Enable','off');
guidata(hObject,handles);


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.uipanel10,'Visible','off');
set(handles.uipanel5,'Visible','on');
set(handles.pushbutton7,'Enable','on');
set(handles.edit7,'String','');
set(handles.edit8,'String','');
set(handles.edit9,'String','');
set(handles.edit10,'String','');
set(handles.edit11,'String','');
set(handles.edit12,'String','');
set(handles.edit13,'String','');
set(handles.edit14,'String','');
guidata(hObject,handles);


% --------------------------------------------------------------------
function uipushtool1_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cla(handles.axes1);
set(handles.axes1,'Visible','off');
set(handles.uipanel4,'Visible','off');
set(handles.uipanel5,'Visible','off');
set(handles.uipanel8,'Visible','off');
set(handles.uipanel9,'Visible','off');
set(handles.uipanel10,'Visible','off');
set(handles.uipanel12,'Visible','off');
set(handles.uipanel13,'Visible','off');
set(handles.pushbutton1,'Visible','off');
set(handles.pushbutton2,'Visible','on');
set(handles.pushbutton3,'Visible','off');
set(handles.pushbutton4,'Visible','on');
set(handles.pushbutton4,'Enable','off');
set(handles.pushbutton6,'Visible','on');
set(handles.pushbutton6,'Enable','off');
set(handles.pushbutton7,'Visible','on');
set(handles.pushbutton7,'Enable','on');
set(handles.pushbutton9,'Enable','off');
set(handles.radiobutton1,'Value',0);
set(handles.radiobutton2,'Value',0);
set(handles.radiobutton3,'Value',0);
set(handles.radiobutton4,'Value',0);
set(handles.radiobutton5,'Value',0);
set(handles.radiobutton6,'Value',0);
set(handles.checkbox1,'Value',0);
set(handles.checkbox2,'Value',0);
set(handles.checkbox3,'Value',0);
set(handles.checkbox4,'Value',0);
set(handles.checkbox1,'Enable','on');
set(handles.checkbox2,'Enable','on');
set(handles.checkbox3,'Enable','on');
set(handles.checkbox4,'Enable','on');
set(handles.edit1,'String','');
set(handles.edit2,'String','');
set(handles.edit3,'String','');
set(handles.edit4,'String','');
set(handles.edit5,'String','');
set(handles.edit6,'String','');
set(handles.edit7,'String','');
set(handles.edit8,'String','');
set(handles.edit9,'String','');
set(handles.edit10,'String','');
set(handles.edit11,'String','');
set(handles.edit12,'String','');
set(handles.edit13,'String','');
set(handles.edit14,'String','');
set(handles.edit23,'String','');
set(handles.edit24,'String','');
guidata(hObject,handles);

